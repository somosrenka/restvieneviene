__author__ = 'thrashforner'
from .base import *
import pymysql

pymysql.install_as_MySQLdb()

#'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'vieneviene',
        'HOST': 'localhost',
        'USER': 'carlosrb',
        'PASSWORD': 'elnanoz1!',
        'PORT': '3306',
    }
}
#'''

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'vieneviene.sqlite3'),
    }
}
'''

########## configuracion para la parte REST del proyecto

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAuthenticated',), ## para que solo puedan acceder a la api los que sean Admins
    'DEFAULT_AUTHENTICATION_CLASSES' : ('rest_framework.authentication.TokenAuthentication',),
    'PAGE_SIZE': 10,
}
REST_SESSION_LOGIN=True


#'DEFAULT_PERMISSION_CLASSES': [
#'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
#'oauth2_provider.ext.rest_framework.OAuth2Authentication',
#'rest_framework_social_oauth2.authentication.SocialAuthentication',

#from rest_auth.serializers import UserDetailsSerializer
#from rest_auth.serializers import LoginSerializer

