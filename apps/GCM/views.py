from django.db import transaction
from rest_framework import generics, status
from push_notifications.models import GCMDevice
from apps.GCM.serializers import GCMDeviceSerializer_
from rest_framework.response import Response

#GCMDevice
from apps.usuarios.models import Usuario


class GCMDeviceList(generics.ListCreateAPIView):
    queryset = GCMDevice.objects.all()
    serializer_class = GCMDeviceSerializer_

    def pre_save(self, obj):
        print("METODO PRE_SAVE GCMDeviceList")
        print(self)
        print(obj)
        obj.user = self.request.user

    #Metodo custom para crear el objeto Auto y el objeto Propietario del Auto
    def create(self, request, *args, **kwargs):
        print("METODO CREATE")
        print(self)
        print(request.user)
        print("DATA ")
        data = request.data
        print(data)
        if (('registration_id' in data)):
            with transaction.atomic():
                #creando el propietario despues de haber salvado el Auto
                gcmdevice, creado = GCMDevice.objects.update_or_create(user=request._user)
                gcmdevice.registration_id = data["registration_id"]
                gcmdevice.save()
                print("Se guardo el objeto, Creado : {0}".format(creado))
            serializer = GCMDeviceSerializer_(gcmdevice, context={'request': request})
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        print("MAL FORMADA LA PETICION, RETORNAMOS 400")
        return Response("ERROR: ni si quiera sabe que valores enviar ", status=status.HTTP_400_BAD_REQUEST)


class GCMDeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = GCMDevice.objects.all()
    serializer_class = GCMDeviceSerializer_
    #patch es para actualizar objeto


"""
class AutosList(generics.ListCreateAPIView):
    queryset = Auto.objects.all()
    serializer_class = AutoSerializer

    #METODO LIST
    def list(self, request):
        '''
        print("METODO LIST")
        print(self)
        print(request)
        print(vars(self))
        print(vars(request))
        print("DATA")
        print(request.data)
        print(request.user)
        '''
        queryset = self.get_queryset()
        serializer = AutoSerializer(queryset, many=True)
        '''
        print("QUERYSET")
        print(queryset)
        print("SERIALIZER")
        print(serializer)
        print(serializer.data)
        '''
        return Response(serializer.data)

    def get_queryset(self):
        print(vars(self))
        user = self.request.user
        propietario = Propietario.objects.filter(dueno=user)
        autos_list = propietario.values_list("auto", flat=True).distinct()
        autos= Auto.objects.filter(id__in=autos_list)
        print(autos_list)
        print(autos)
        return autos

    #Metodo custom para crear el objeto Auto y el objeto Propietario del Auto
    def create(self, request, *args, **kwargs):
        print("METODO CREATE")
        print(self)
        print(request.user)
        print(vars(self))
        print(vars(request))
        print(args)
        print(kwargs)
        print("DATA ")
        data = request.data
        print(data)
        if(('color' in data) and ('marca' in data) and ('placas' in data)):
            #placas=data['placas']
            #color=data['color']
            #marca=data['marca']
            #VALIDANDO QUE LOS CAMPOS NO ESTEN VACIOS
            #if((placas !='') and (color!='') and (marca!='')):
            # note transaction.atomic was introduced in Django 1.6
            with transaction.atomic():
                auto = Auto(placas=data['placas'],color=data['color'],
                             marca=data['marca'] )
                auto.clean()
                auto.save()
                #creando el propietario despues de haber salvado el Auto
                Propietario.objects.create(dueno=request._user, auto=auto)
            serializer = AutoSerializer(auto)
            headers = self.get_success_headers(serializer.data)
            print("HEADERS A RETORNAR AL SERVIDOR")
            print(headers)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        print("MAL FORMADA LA PETICION, RETORNAMOS 400")
        return Response("ERROR: ni si quiera sabe que valores enviar ", status=status.HTTP_400_BAD_REQUEST)



class AutosDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Auto.objects.all()
    serializer_class = AutoSerializer
    #patch es para actualizar objeto
"""