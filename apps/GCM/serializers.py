from rest_framework.exceptions import ValidationError
from apps.usuarios.models import Usuario
from apps.usuarios.serializers import UserSerializer

__author__ = 'thrashforner'
from rest_framework import serializers
from push_notifications.models import GCMDevice
#GCMDevice

# Serializers define the API representation.
class GCMDeviceSerializer_(serializers.HyperlinkedModelSerializer):
    #user = serializers.EmailField(source='user.email', read_only=True)
    #user = serializers.EmailField(source='user.email')
    #user = serializers.RelatedField(source='user.email')
    #user = serializers.Field(source='user.email')
    user = serializers.EmailField( source='user.email')

    class Meta:
        model = GCMDevice
        fields = ('id', 'device_id', 'registration_id', 'user')

    '''

    def to_internal_value(self, data):
        print("TO INTERNAL VALUE")
        print(self)
        print(data)
        print(type(data))
        email_user = data.get('user')
        registration_id = data.get('registration_id')

        if not registration_id:
            raise ValidationError({ 'registration_id': 'El ID de registro es requerido.'})
        # Perform the data validation.
        if not email_user:
            raise ValidationError({'email': 'El email es requerido.'})
        else:
            try:
                data["user"] = Usuario.objects.get(email=email_user)
            except Exception as e:
                print(e)
                raise ValidationError({'user': 'No existe el usuario.'})
        # the `.validated_data` property.
        return data

    def to_representation(self, obj):
        return {'user': obj.user.email, 'registration_id': obj.registration_id }

    #OVERRIDE
    def create(self, validated_data):
        print("METODO CREATE EN GCMDeviceSerializer_")
        print("SELF")
        print(self)
        print("VALIDATED_DATA")
        print(validated_data)
        print(validated_data["user"])
    '''

