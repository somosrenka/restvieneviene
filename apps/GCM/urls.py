__author__ = 'thrashforner'
from django.conf.urls import url
from apps.GCM.views import GCMDeviceList, GCMDeviceDetail
#from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    #url(r'^/$', views.SnippetList.as_view()),
    url(r'^regid/$', GCMDeviceList.as_view()),
    url(r'^regid/(?P<pk>[0-9]+)/$', GCMDeviceDetail.as_view()),
]



