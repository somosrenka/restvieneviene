__author__ = 'thrashforner'


from gcm import GCM


API_KEY="AIzaSyCb--nEn6x4S_aIfhldJeyF6SlZ0cJdTcY"
gcm = GCM(API_KEY)
data = {'param1': 'value1', 'param2': 'value2'}

# Downstream message using JSON request
reg_ids = ['token1', 'token2', 'token3']
response = gcm.json_request(registration_ids=reg_ids, data=data)

# Downstream message using JSON request with extra arguments
res = gcm.json_request(
    registration_ids=reg_ids, data=data,
    collapse_key='uptoyou', delay_while_idle=True, time_to_live=3600
)

# Topic Messaging
topic = 'topic name'
gcm.send_topic_message(topic=topic, data=data)