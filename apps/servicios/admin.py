from django.contrib import admin
from apps.servicios.models import AreaServicio, Ubicacion


# Register your models here.

class CordenadasInline(admin.TabularInline):
    model = Ubicacion
    extra = 0


@admin.register(AreaServicio)
class AdminAreaServicio(admin.ModelAdmin):
    fields = ("descripcion",)
    list_display = ("descripcion", "fecha")
    inlines = (CordenadasInline,)
