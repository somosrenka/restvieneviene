from apps.servicios.views import ServicioDetail, ServiciosList, FotoDetail, FotosList, \
    TicketDetail, TicketsList, AreaServiciosList, AreaServicioDetail

__author__ = 'thrashforner'
from django.conf.urls import url
#from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    #url(r'^/$', views.SnippetList.as_view()),
    url(r'^tickets/$', TicketsList.as_view()),
    url(r'^tickets/(?P<pk>[0-9]+)/$', TicketDetail.as_view()),

    url(r'^fotos/$', FotosList.as_view()),
    url(r'^fotos/(?P<pk>[0-9]+)/$', FotoDetail.as_view()),

    url(r'^servicio/$', ServiciosList.as_view()),
    url(r'^servicio/(?P<pk>[0-9]+)/$', ServicioDetail.as_view()),


    url(r'^area_servicio/$', AreaServiciosList.as_view()),
    url(r'^area_servicio/(?P<pk>[0-9]+)/$', AreaServicioDetail.as_view()),

    #url(r'^ubicacion/$', UbicacionesList.as_view()),
    #url(r'^ubicacion/(?P<pk>[0-9]+)/$', UbicacionDetail.as_view()),

]

#urlpatterns = format_suffix_patterns(urlpatterns)