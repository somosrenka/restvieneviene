# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-13 17:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('autos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AreaServicio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.TextField(blank=True, max_length=3000, null=True)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Foto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valet1', models.IntegerField(default=0)),
                ('valet2', models.IntegerField(default=0)),
                ('latitud', models.FloatField()),
                ('longitud', models.FloatField()),
                ('estado', models.CharField(choices=[('solicitado', 'solicitado'), ('entregadoAValet', 'entregadoAValet'), ('estacionado', 'estacionado'), ('pedido', 'pedido'), ('entregadoAUsuario', 'entregadoAUsuario'), ('cancelado', 'cancelado')], default='solicitado', max_length=20)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('aritulos', models.TextField(max_length=3000)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('propietario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='autos.Propietario')),
            ],
        ),
        migrations.CreateModel(
            name='Ubicacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitud', models.FloatField()),
                ('longitud', models.FloatField()),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('areaServicio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicios.AreaServicio')),
            ],
        ),
    ]
