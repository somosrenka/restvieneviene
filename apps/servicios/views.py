from itertools import product
from django.db import transaction
from rest_framework import generics, status
from apps.servicios.models import Ticket, Foto, Servicio, AreaServicio
from apps.servicios.serializers import TicketSerializer, FotoSerializer, ServicioSerializer, AreaServicioSerializer
from rest_framework.response import Response
from apps.autos.models import Propietario, Auto
# Create your views here.
from apps.usuarios.models import Usuario

""" SERIALIZERS DE TICKETS """

class TicketsList(generics.ListCreateAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer


class TicketDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    #patch es para actualizar objeto


""" SERIALIZERS DE FOTOS """

class FotosList(generics.ListCreateAPIView):
    queryset = Foto.objects.all()
    serializer_class = FotoSerializer


class FotoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Foto.objects.all()
    serializer_class = FotoSerializer
    #patch es para actualizar objeto


""" SERIALIZERS DE SERVICIOS """

class ServiciosList(generics.ListCreateAPIView):
    queryset = Servicio.objects.all()
    serializer_class = ServicioSerializer

    #Nota para Carlos
    #   redefinir metodo para obtener, modificar o eliminar servicio


    #METODO LIST
    def list(self, request):
        '''
        print("METODO LIST")
        print(self)
        print(request)
        print(vars(self))
        print(vars(request))
        print("DATA")
        print(request.data)
        print(request.user)
        '''
        queryset = self.get_queryset()
        serializer = ServicioSerializer(queryset, many=True)
        '''
        print("QUERYSET")
        print(queryset)
        print("SERIALIZER")
        print(serializer)
        print(serializer.data)
        '''
        return Response(serializer.data)

    def get_queryset(self):
        print(vars(self))
        user = self.request.user
        propietario = Propietario.objects.filter(dueno=user)
        servicios= Servicio.objects.filter(propietario=propietario)
        print(servicios)
        return servicios


    #Metodo custom para crear el servicio y y crear las demas cosas necesarias
    #Se creara de forma CUSTOM ell servicio, pues se debe de 'amarrar' a un usuario, a una ubocacion
    #y asignarse un estado
    #"""
    def create(self, request, *args, **kwargs):
        print("METODO CREATE DE SERVICIOS")
        print(self)
        print(request.user)
        data = request.data
        print("DATA ")
        print(data)
        print(type(data))
        msg="ERROR: PROBANDO COMO ENVBIAR PETICIONES DE PEDIDO DE CARRO"
        if(('latitud' in data) and ('longitud' in data) and ('placas' in data)):
            latitud=data['latitud']
            longitud=data['longitud']
            placas=data['placas']
            #VALIDANDO QUE LOS CAMPOS NO ESTEN VACIOS
            #if((placas !='') and (latitud!='') and (longitud!='') ):
            # Asignando el servicio
            try:
                with transaction.atomic():
                    #buscamos al propietario
                    auto = Auto.objects.get(placas=placas)
                    propietario = Propietario.objects.get(dueno=request.user, auto=auto)
                    #   Creamos objeto Servicio despues de haber obtenido lo necesario
                    print("AUTO: ")
                    print(auto)
                    print("Propietario: ")
                    print(propietario)
                    servicio = Servicio.objects.create(propietario=propietario,
                                                       latitud=latitud, longitud=longitud)
                    #**** SE MANDARAN NOTIFICACIONES A LOS USUARIOS DE TIPO VALET PARA ACEPTAR EL SERVICIO ***#

                    #valets_ = Usuario.objects.filter(is_valet=True)

                    valets_ = Usuario.objects.all().exclude(id=request.user.id)
                    valets_id = [v.id for v in valets_]
                    #Intentando obtener GSM's de los usuarios
                    from push_notifications.models import GCMDevice
                    devices_ = GCMDevice.objects.filter(user__id__in=valets_id)
                    print("devices_")
                    print(devices_)
                    for d in devices_:
                        print("device.registration_id")
                        print(d.registration_id)
                        #enviando la notificacion
                        response = d.send_message("You've got mail", extra={"foo": "bar"})
                        print("response")
                        print(response)
                    print("se enviaron las notificaciones")


                    '''
                    #   PROBANDO ENVIO DE MENSAJES GCM
                    regid = "evZBldtibjY:APA91bGXCCZFJWkrSsBw3f6RQWO6bzvUL_o-kOLn30POiZXKHtM-hdeUzr0q7u_xUTnJidZw9cJN8k9pIpPJrVdvigVmO9RnbU3Gv8MdV2JchbXaDk5DuRhBni3G6HaibGzJmZS_7giB"
                    from push_notifications.models import GCMDevice
                    device, created = GCMDevice.objects.get_or_create(registration_id=regid, user=request.user)
                    print("device")
                    print(device)
                    print(dir(device))
                    #device.send_message(None, extra={"foo": "bar"})
                    device.send_message("You've got mail", extra={"foo": "bar"})
                    print("SE ENVIARON MENSAJES")
                    '''


                serializer = ServicioSerializer(servicio, context={'request': request})
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

            except Exception as e:
                print(e)
                msg=str(e)
        print("MAL FORMADA LA PETICION, RETORNAMOS 400")
        return Response(msg, status=status.HTTP_400_BAD_REQUEST)
    """
    #"""



class ServicioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Servicio.objects.all()
    serializer_class = ServicioSerializer
    #patch es para actualizar objeto



""" SERIALIZERS DE UBICACION """

'''
class UbicacionesList(generics.ListCreateAPIView):
    queryset = Ubicacion.objects.all()
    serializer_class = UbicacionSerializer


    def create(self, request, *args, **kwargs):
        print("METODO CREATE DE SERVICIOS")
        print(self)
        print(request.user)
        print("DATA ")
        data = request.data
        print(data)
        if(('color' in data) and ('marca' in data) and ('placas' in data)):
            #placas=data['placas']
            #color=data['color']
            #marca=data['marca']
            #VALIDANDO QUE LOS CAMPOS NO ESTEN VACIOS
            #if((placas !='') and (color!='') and (marca!='')):
            # Asignando el servicio
            with transaction.atomic():
                ubicacion = Ubicacion.objects.create()


                #buscamos al propietario
                propietario = Propietario.objects.get(dueno=request.user)
                #   Creamos objeto Servicio despues de haber obtenido lo necesario
                servicio = Servicio.objects.create(propietario=propietario)

                #**** SE MANDARAN NOTIFICACIONES A LOS USUARIOS DE TIPO VALET PARA ACEPTAR EL SERVICIO ***#


            serializer = ServicioSerializer(servicio)
            headers = self.get_success_headers(serializer.data)
            print("HEADERS A RETORNAR AL SERVIDOR")
            print(headers)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        print("MAL FORMADA LA PETICION, RETORNAMOS 400")
        return Response("ERROR: PROBANDO COMO ENVBIAR PETICIONES DE PEDIDO DE CARRO ", status=status.HTTP_400_BAD_REQUEST)


class UbicacionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ubicacion.objects.all()
    serializer_class = UbicacionSerializer
    #patch es para actualizar objeto
'''

""" SERIALIZERS DE AREASSERVICIOS / UBICACION"""

class AreaServiciosList(generics.ListCreateAPIView):
    queryset = AreaServicio.objects.all()
    serializer_class = AreaServicioSerializer

    """
    List a queryset.
    """
    def list(self, request, *args, **kwargs):
        print("LIST")
        queryset = self.filter_queryset(self.get_queryset())
        print(queryset)

        page = self.paginate_queryset(queryset)
        print(page)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            print("Serializer en IF")
            print(serializer)
            print("_________________")
            print(serializer.data)
            print("***********************************")
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        print("serializer fuera de if")
        print(serializer)
        return Response(serializer.data)


    def get(self, request, *args, **kwargs):
        print("metodo GET en ListCreateAPIView")
        print(vars(request))
        print(args)
        print(kwargs)
        print("DATA")
        print(request.data)
        list = self.list(request, *args, **kwargs)
        print("LIST en METODO GET")
        print(list)
        return list



class AreaServicioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AreaServicio.objects.all()
    serializer_class = AreaServicioSerializer
    #patch es para actualizar objeto
