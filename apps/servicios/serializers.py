__author__ = 'thrashforner'
from apps.servicios.models import Ticket, Foto, Servicio, AreaServicio, Ubicacion
from rest_framework import serializers


# Serializers define the API representation.
class TicketSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ticket
        fields = ('valet', 'propietario', 'articulos')


# Serializers define the API representation.
class FotoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Foto
        fields = ('ticket', 'foto')

# Serializers define the API representation.
class ServicioSerializer(serializers.HyperlinkedModelSerializer):
    placas = serializers.CharField(source="propietario.auto.placas", read_only=True)
    class Meta:
        model = Servicio
        fields = ('id', 'latitud', 'longitud', 'valet1', 'valet2', 'placas')

# Serializers define the API representation.
class UbicacionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ubicacion
        fields = ('latitud', 'longitud')

# Serializers define the API representation.
class AreaServicioSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer(source="get_ubications", read_only=True, many=True)
    #ubicacion = serializers.CharField(source="Ubicacion", read_only=True)

    class Meta:
        model = AreaServicio
        fields = ('descripcion', 'ubicacion')
