from django.db import models

# Create your models here.
from apps.autos.models import Propietario
from apps.usuarios.models import PerfilValet


class Ticket(models.Model):
    valet = models.ForeignKey(PerfilValet)
    propietario = models.ForeignKey(Propietario)
    aritulos = models.TextField(max_length=3000)
    fecha = models.DateTimeField(auto_now_add=True)


class Foto(models.Model):
    ticket = models.ForeignKey(Ticket)
    foto = models.ImageField
    fecha = models.DateTimeField(auto_now_add=True)


class Servicio(models.Model):
    # ubicacion = models.ForeignKey(Ubicacion)
    propietario = models.ForeignKey(Propietario)
    valet1 = models.IntegerField(default=0)
    valet2 = models.IntegerField(default=0)
    lista_estados = (
        ("solicitado", "solicitado"),
        ("entregadoAValet", "entregadoAValet"),
        ("estacionado", "estacionado"),
        ("pedido", "pedido"),
        ("entregadoAUsuario", "entregadoAUsuario"),
        ("cancelado", "cancelado"),
    )
    latitud = models.FloatField()
    longitud = models.FloatField()
    estado = models.CharField(choices=lista_estados, max_length=20, default="solicitado")
    fecha = models.DateTimeField(auto_now_add=True)


class AreaServicio(models.Model):
    descripcion = models.TextField(max_length=3000, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0} ".format(str(self.descripcion))


    def get_ubications(self):
        print("GET_UBICATIONS")
        ubicaciones = Ubicacion.objects.filter(areaServicio=self)
        return ubicaciones

class Ubicacion(models.Model):
    areaServicio = models.ForeignKey(AreaServicio)
    latitud = models.FloatField()
    longitud = models.FloatField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0} : {1}".format(str(self.latitud),str(self.longitud))