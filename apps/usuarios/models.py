from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models


def directorio_IFE(instance, filename):
    return "usuarios/{id}/{file}".format(id=instance.usuario.id, file=filename)


def directorio_Carnet(instance, filename):
    return "usuarios/{id}/{file}".format(id=instance.usuario.id, file=filename)

class UserManager(BaseUserManager, models.Manager):
    def _create_user( self, username, email, password, is_staff,
                      is_superuser, is_active=False, **extra_fields):
        print("METODO _create_user")
        print(username,email,password)
        email = self.normalize_email(email)
        # if not email:
        # raise ValueError('El email debe ser obligatorio')
        user = self.model( username=email, email=email, is_active=is_active,
                           is_staff=is_staff, is_superuser=is_superuser, **extra_fields )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password=None, is_active=True, **extra_fields):
        print("CREANDO USUARIO MORTAL")
        print(username, email, password)
        print(extra_fields)
        return self._create_user(username=email, email=email, password=password,
                                 is_staff=False, is_superuser=False, is_active=True, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        print("CREANDO SUPER USUARIO")
        print(username, email, password)
        print(extra_fields)
        return self._create_user(username=email, email=email, password=password,
                                 is_staff=True, is_superuser=True, is_active=True, **extra_fields)

#import django.contrib.auth.management.commands
class Usuario(AbstractBaseUser, PermissionsMixin):
    nombre = models.CharField(max_length=40, blank=True)
    apellidos = models.CharField(max_length=40, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(unique=True)
    avatar = models.URLField(blank=True)
    lista_acceso = (
        ('Datos', 'Datos'),
        ('Facebook', 'Facebook'),
        ('Google', 'Google'),
    )
    acceso = models.CharField(choices=lista_acceso, max_length=10, default="Datos")
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_valet = models.BooleanField(default=False)
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']


    @property
    def get_short_name(self):
        if self.nombre != "":
            return self.nombre
        else:
            return self.email

    @property
    def get_full_name(self):
        if self.nombre != "":
            return self.nombre + " " + self.apellidos
        else:
            return str(self.email)


class PerfilValet(models.Model):
    usuario = models.OneToOneField(Usuario, related_name="Usuario", primary_key=True)
    IFE = models.ImageField(upload_to=directorio_IFE)
    direccion = models.CharField(max_length=500)
    noControl = models.CharField(max_length=100)
    permisoConducir = models.ImageField(upload_to=directorio_Carnet)
    fecha = models.DateTimeField(auto_now_add=True)
