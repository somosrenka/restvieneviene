__author__ = 'thrashforner'
from django.contrib.auth.models import Group
from apps.usuarios.models import Usuario
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    #email = serializers.EmailField(allow_blank=False)

    class Meta:
        model = Usuario
        fields = ('id', 'email', 'avatar')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

