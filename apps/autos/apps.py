from django.apps import AppConfig


class AutosConfig(AppConfig):
    name = 'apps.autos'
