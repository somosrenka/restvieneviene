__author__ = 'thrashforner'

from rest_framework import routers, serializers, viewsets


from apps.autos.models import Auto, Propietario
# Serializers define the API representation.
class AutoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Auto
        fields = ('placas', 'color','marca','id')


# Serializers define the API representation.
class PropietarioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Propietario
        fields = ('dueno', 'auto', 'fecha')

